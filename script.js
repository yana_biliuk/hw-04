// task 1

function showNumber(value) {
  return isFinite(value);
}

let num = parseFloat(prompt("Ведіть перше значення:"));
while (!showNumber(num)) {
  num = parseFloat(prompt("Введіть число"));
}

let numTwo = parseFloat(prompt("Ведіть друге значення:"));
while (!showNumber(numTwo)) {
  numTwo = parseFloat(prompt("Введіть число"));
}

let min = Math.min(num, numTwo);
let max = Math.max(num, numTwo);

for (let i = min; i <= max; i++) {
  console.log(i);
}

// task 2

let number = prompt("Введіть парне число:");

while (isNaN(number) || number % 2 !== 0) {
  number = prompt("НЕ вірно. Введіть число:");
}
console.log(number);
